using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[ExecuteInEditMode]
public class VoxelLight : MonoBehaviour
{
    [SerializeField]
    [HideInInspector]
    private Light vlight;
    public int nodeFlags;
    public Rayform.Scene.Node.Flags objFlags;
    public int unknown1;
    public Color color { get { return vlight.color; } set { vlight.color = value; } }
    public float multiplier;
    public float attenDist;
    public int unknown3;

    void Init()
    {
        if (vlight == null)
        {
            vlight = gameObject.AddComponent<Light>();
            vlight.shadows = LightShadows.Soft;
        }
        vlight.hideFlags = HideFlags.HideInInspector;
    }

    private void Start()
    {
        Init();
        UpdateLight();
    }

    private void UpdateLight()
    {
        //light.intensity = intensity;
        //light.range = range;
        //light.color = color;        
    }

    private void OnValidate()
    {
        Init();
        UpdateLight();
    }

    public void OnDestroy()
    {
        DestroyImmediate(vlight);
    }

    public static void Import(GameObject obj, Rayform.LightSource lightSource)
    {

    }

    [CustomEditor(typeof(VoxelLight))]
    public class CustomInspector : Editor
    {
        VoxelLight target;

        public override void OnInspectorGUI()
        {
            GUIStyle style = new GUIStyle { richText = true };
            //target.lightFlags = (Rayform.LightSource.LightFlags)EditorGUILayout.EnumPopup("Light Type", target.lightFlags);
            target.objFlags = (Rayform.Scene.Node.Flags)EditorGUILayout.EnumFlagsField("Obj Flags", target.objFlags);
            target.unknown1 = EditorGUILayout.IntField("Unknown1", target.unknown1);
            target.vlight.color = EditorGUILayout.ColorField("Color", target.vlight.color);
            target.multiplier = EditorGUILayout.FloatField("Multiplier", target.multiplier);
            target.attenDist = EditorGUILayout.FloatField("AttenDist", target.attenDist);
            target.unknown3 = EditorGUILayout.IntField("Unknown3", target.unknown3);
        }

        void Awake()
        {
            target = (VoxelLight)serializedObject.targetObject;
            //obj.id = new Rayform.Id();
            //obj.data = new Rayform.Character();
            //Debug.Log(obj.data.name);
        }
    }
}