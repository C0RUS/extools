using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing.Drawing2D;
using System.Reflection;
using UnityEngine;

public static class Utils
{
    public static List<Color32> palette = new List<Color32> {
        new Color32(255, 255, 255, 255),
        new Color32(197, 197, 197, 255),
        new Color32(143, 143, 143, 255),
        new Color32(98, 98, 98, 255),
        new Color32(61, 61, 61, 255),
        new Color32(33, 33, 33, 255),
        new Color32(13, 13, 13, 255),
        new Color32(3, 3, 3, 255),

        new Color32(255, 221, 213, 255),
        new Color32(218, 183, 175, 255),
        new Color32(171, 141, 134, 255),
        new Color32(131, 105, 99, 255),
        new Color32(96, 77, 72, 255),
        new Color32(66, 55, 53, 255),
        new Color32(42, 36, 35, 255),
        new Color32(23, 21, 20, 255),

        new Color32(255, 128, 143, 255),
        new Color32(218, 91, 106, 255),
        new Color32(171, 59, 72, 255),
        new Color32(131, 37, 48, 255),
        new Color32(96, 26, 34, 255),
        new Color32(66, 27, 32, 255),
        new Color32(42, 22, 24, 255),
        new Color32(23, 14, 15, 255),

        new Color32(255, 151, 153, 255),
        new Color32(218, 113, 116, 255),
        new Color32(171, 79, 81, 255),
        new Color32(131, 54, 55, 255),
        new Color32(96, 38, 40, 255),
        new Color32(66, 34, 35, 255),
        new Color32(42, 25, 26, 255),
        new Color32(23, 16, 16, 255),

        new Color32(255, 101, 101, 255),
        new Color32(218, 63, 63, 255),
        new Color32(171, 35, 35, 255),
        new Color32(131, 17, 17, 255),
        new Color32(96, 11, 11, 255),
        new Color32(66, 19, 19, 255),
        new Color32(42, 18, 18, 255),
        new Color32(23, 12, 12, 255),

        new Color32(255, 173, 151, 255),
        new Color32(218, 135, 113, 255),
        new Color32(171, 98, 79, 255),
        new Color32(131, 70, 54, 255),
        new Color32(96, 51, 38, 255),
        new Color32(66, 41, 34, 255),
        new Color32(42, 29, 25, 255),
        new Color32(23, 17, 16, 255),

        new Color32(255, 208, 182, 255),
        new Color32(218, 170, 145, 255),
        new Color32(171, 129, 107, 255),
        new Color32(131, 96, 77, 255),
        new Color32(96, 70, 56, 255),
        new Color32(66, 51, 44, 255),
        new Color32(42, 34, 30, 255),
        new Color32(23, 20, 18, 255),

        new Color32(255, 199, 158, 255),
        new Color32(218, 162, 120, 255),
        new Color32(171, 122, 85, 255),
        new Color32(131, 89, 59, 255),
        new Color32(96, 65, 42, 255),
        new Color32(66, 49, 36, 255),
        new Color32(42, 33, 26, 255),
        new Color32(23, 19, 16, 255),

        new Color32(255, 177, 101, 255),
        new Color32(218, 140, 63, 255),
        new Color32(171, 102, 35, 255),
        new Color32(131, 73, 17, 255),
        new Color32(96, 53, 11, 255),
        new Color32(66, 42, 19, 255),
        new Color32(42, 29, 18, 255),
        new Color32(23, 17, 12, 255),

        new Color32(255, 213, 152, 255),
        new Color32(218, 175, 115, 255),
        new Color32(171, 134, 80, 255),
        new Color32(131, 99, 55, 255),
        new Color32(96, 72, 39, 255),
        new Color32(66, 53, 35, 255),
        new Color32(42, 35, 26, 255),
        new Color32(23, 20, 16, 255),

        new Color32(255, 209, 130, 255),
        new Color32(218, 172, 92, 255),
        new Color32(171, 131, 60, 255),
        new Color32(131, 97, 38, 255),
        new Color32(96, 70, 27, 255),
        new Color32(66, 52, 28, 255),
        new Color32(42, 34, 22, 255),
        new Color32(23, 20, 14, 255),

        new Color32(255, 236, 195, 255),
        new Color32(218, 199, 158, 255),
        new Color32(171, 155, 118, 255),
        new Color32(131, 117, 87, 255),
        new Color32(96, 85, 63, 255),
        new Color32(66, 60, 48, 255),
        new Color32(42, 39, 32, 255),
        new Color32(23, 22, 19, 255),

        new Color32(255, 220, 101, 255),
        new Color32(218, 183, 63, 255),
        new Color32(171, 141, 35, 255),
        new Color32(131, 105, 17, 255),
        new Color32(96, 77, 11, 255),
        new Color32(66, 55, 19, 255),
        new Color32(42, 36, 18, 255),
        new Color32(23, 20, 12, 255),

        new Color32(255, 241, 177, 255),
        new Color32(218, 204, 140, 255),
        new Color32(171, 159, 102, 255),
        new Color32(131, 121, 73, 255),
        new Color32(96, 88, 53, 255),
        new Color32(66, 62, 42, 255),
        new Color32(42, 39, 29, 255),
        new Color32(23, 22, 17, 255),

        new Color32(255, 239, 101, 255),
        new Color32(218, 201, 63, 255),
        new Color32(171, 157, 35, 255),
        new Color32(131, 119, 17, 255),
        new Color32(96, 87, 11, 255),
        new Color32(66, 61, 19, 255),
        new Color32(42, 39, 18, 255),
        new Color32(23, 22, 12, 255),

        new Color32(251, 255, 140, 255),
        new Color32(214, 218, 103, 255),
        new Color32(168, 171, 70, 255),
        new Color32(128, 131, 46, 255),
        new Color32(93, 96, 33, 255),
        new Color32(65, 66, 31, 255),
        new Color32(41, 42, 24, 255),
        new Color32(23, 23, 15, 255),

        new Color32(201, 224, 162, 255),
        new Color32(161, 183, 123, 255),
        new Color32(126, 145, 92, 255),
        new Color32(96, 112, 68, 255),
        new Color32(73, 84, 53, 255),
        new Color32(53, 59, 41, 255),
        new Color32(35, 39, 29, 255),
        new Color32(21, 23, 18, 255),

        new Color32(143, 183, 120, 255),
        new Color32(112, 150, 90, 255),
        new Color32(87, 121, 66, 255),
        new Color32(67, 96, 51, 255),
        new Color32(54, 73, 43, 255),
        new Color32(41, 53, 34, 255),
        new Color32(29, 36, 25, 255),
        new Color32(19, 23, 17, 255),

        new Color32(114, 183, 132, 255),
        new Color32(84, 150, 101, 255),
        new Color32(61, 121, 77, 255),
        new Color32(46, 96, 59, 255),
        new Color32(40, 73, 48, 255),
        new Color32(32, 53, 38, 255),
        new Color32(24, 36, 27, 255),
        new Color32(16, 23, 18, 255),

        new Color32(75, 145, 108, 255),
        new Color32(53, 121, 85, 255),
        new Color32(37, 100, 67, 255),
        new Color32(33, 80, 55, 255),
        new Color32(29, 62, 45, 255),
        new Color32(25, 47, 35, 255),
        new Color32(19, 34, 26, 255),
        new Color32(14, 23, 18, 255),

        new Color32(105, 183, 169, 255),
        new Color32(76, 150, 137, 255),
        new Color32(54, 121, 109, 255),
        new Color32(40, 96, 86, 255),
        new Color32(36, 73, 66, 255),
        new Color32(30, 53, 49, 255),
        new Color32(22, 36, 34, 255),
        new Color32(15, 23, 22, 255),

        new Color32(105, 172, 183, 255),
        new Color32(76, 140, 150, 255),
        new Color32(54, 112, 121, 255),
        new Color32(40, 88, 96, 255),
        new Color32(36, 67, 73, 255),
        new Color32(30, 50, 53, 255),
        new Color32(22, 34, 36, 255),
        new Color32(15, 22, 23, 255),

        new Color32(101, 188, 255, 255),
        new Color32(63, 151, 218, 255),
        new Color32(35, 112, 171, 255),
        new Color32(17, 81, 131, 255),
        new Color32(11, 59, 96, 255),
        new Color32(19, 45, 66, 255),
        new Color32(18, 31, 42, 255),
        new Color32(12, 18, 23, 255),

        new Color32(134, 197, 255, 255),
        new Color32(97, 159, 218, 255),
        new Color32(64, 120, 171, 255),
        new Color32(41, 88, 131, 255),
        new Color32(30, 64, 96, 255),
        new Color32(29, 48, 66, 255),
        new Color32(23, 33, 42, 255),
        new Color32(14, 19, 23, 255),

        new Color32(177, 208, 255, 255),
        new Color32(139, 171, 218, 255),
        new Color32(102, 130, 171, 255),
        new Color32(73, 96, 131, 255),
        new Color32(53, 70, 96, 255),
        new Color32(42, 51, 66, 255),
        new Color32(29, 34, 42, 255),
        new Color32(17, 20, 23, 255),

        new Color32(117, 152, 255, 255),
        new Color32(79, 115, 218, 255),
        new Color32(48, 80, 171, 255),
        new Color32(28, 55, 131, 255),
        new Color32(20, 39, 96, 255),
        new Color32(24, 35, 66, 255),
        new Color32(20, 26, 42, 255),
        new Color32(13, 16, 23, 255),

        new Color32(122, 142, 255, 255),
        new Color32(84, 104, 218, 255),
        new Color32(53, 71, 171, 255),
        new Color32(32, 47, 131, 255),
        new Color32(23, 34, 96, 255),
        new Color32(25, 31, 66, 255),
        new Color32(21, 24, 42, 255),
        new Color32(13, 15, 23, 255),

        new Color32(192, 182, 255, 255),
        new Color32(155, 145, 218, 255),
        new Color32(116, 107, 171, 255),
        new Color32(84, 77, 131, 255),
        new Color32(61, 56, 96, 255),
        new Color32(47, 44, 66, 255),
        new Color32(32, 30, 42, 255),
        new Color32(18, 18, 23, 255),

        new Color32(134, 94, 183, 255),
        new Color32(103, 65, 150, 255),
        new Color32(79, 44, 121, 255),
        new Color32(61, 32, 96, 255),
        new Color32(49, 30, 73, 255),
        new Color32(38, 26, 53, 255),
        new Color32(28, 20, 36, 255),
        new Color32(18, 14, 23, 255),

        new Color32(173, 129, 183, 255),
        new Color32(141, 99, 150, 255),
        new Color32(113, 74, 121, 255),
        new Color32(89, 57, 96, 255),
        new Color32(68, 47, 73, 255),
        new Color32(50, 37, 53, 255),
        new Color32(35, 27, 36, 255),
        new Color32(22, 18, 23, 255),

        new Color32(255, 203, 247, 255),
        new Color32(218, 166, 210, 255),
        new Color32(171, 125, 164, 255),
        new Color32(131, 92, 125, 255),
        new Color32(96, 67, 91, 255),
        new Color32(66, 50, 63, 255),
        new Color32(42, 34, 40, 255),
        new Color32(23, 19, 22, 255),

        new Color32(183, 105, 134, 255),
        new Color32(150, 75, 104, 255),
        new Color32(121, 53, 79, 255),
        new Color32(96, 40, 61, 255),
        new Color32(73, 35, 49, 255),
        new Color32(53, 29, 38, 255),
        new Color32(36, 22, 28, 255),
        new Color32(23, 15, 18, 255),
    };

    public static Vector2 ConvertVector2(this Rayform.Vector2 vector)
    {
        return new Vector2(vector.x, vector.y);
    }

    public static Vector3 ConvertVector3(this Rayform.Vector3 vector)
    {
        return new Vector3(-vector.x / 100, vector.z / 100, vector.y / 100);
    }

    public static Rayform.Vector3 ConvertVector3(this Vector3 vector)
    {
        return new Rayform.Vector3(-vector.x * 100, vector.z * 100, vector.y * 100);
    }

    public static Color ConvertColor(this Rayform.Color color)
    {
        return new Color(color.r, color.g , color.b, color.a);
    }

    public static Rayform.Color ConvertColor(this Color color)
    {
        return new Rayform.Color(color.r, color.g, color.b, color.a);
    }

    public static void Reset(this Transform transform)
    {
        transform.localEulerAngles = Vector3.zero;
        transform.localPosition = Vector3.zero;
        transform.localScale = Vector3.one;
    }

    public static Matrix4x4 ConvertTo4x4(this Rayform.Matrix3x4 matrix3x4)
    {
        Matrix4x4 matrix4x4 = new Matrix4x4();

        //x axis
        matrix4x4.m00 = matrix3x4.m00;
        matrix4x4.m01 = -matrix3x4.m10;
        matrix4x4.m02 = -matrix3x4.m20;

        //y axis
        matrix4x4.m10 = -matrix3x4.m01;
        matrix4x4.m11 = matrix3x4.m11;
        matrix4x4.m12 = matrix3x4.m21;

        //z axis
        matrix4x4.m20 = -matrix3x4.m02;
        matrix4x4.m21 = matrix3x4.m12;
        matrix4x4.m22 = matrix3x4.m22;

        //xyz translation
        matrix4x4.m03 = -matrix3x4.m30 / 100;
        matrix4x4.m13 = matrix3x4.m31 / 100;
        matrix4x4.m23 = matrix3x4.m32 / 100;
        matrix4x4.m33 = 1;

        //xyz translation
        //matrix4x4.m03 = matrix3x4.m30 / 100;
        //matrix4x4.m13 = matrix3x4.m31 / 100;
        //matrix4x4.m23 = -matrix3x4.m32 / 100;
        //matrix4x4.m33 = 1;

        //float width = 8;
        //silly scale is inverted
        //var rotation = matrix4x4.ExtractRotation().eulerAngles;
        //var position = matrix4x4.ExtractPosition();
        //
        //position -= rotation.normalized * (matrix4x4.lossyScale.x -1);
        //matrix4x4.m03 = position.x;
        //matrix4x4.m13 = position.y;
        //matrix4x4.m23 = position.z;

        //position.Scale()
        //matrix4x4.m03 -= matrix4x4.m23 * matrix4x4.lossyScale.x;



        return matrix4x4;
    }

    public static Rayform.Matrix3x4 ConvertTo3x4(this Matrix4x4 matrix4x4)
    {
        Rayform.Matrix3x4 matrix3x4 = new Rayform.Matrix3x4();

        //x axis
        matrix3x4.m00 = matrix4x4.m00;
        matrix3x4.m10 = -matrix4x4.m01; 
        matrix3x4.m20 = -matrix4x4.m02; 
                       
        //y axis
        matrix3x4.m01 = -matrix4x4.m10;
        matrix3x4.m11 = matrix4x4.m11;
        matrix3x4.m21 = matrix4x4.m12;
                                       
        //z axis      
        matrix3x4.m02 = -matrix4x4.m20;
        matrix3x4.m12 = matrix4x4.m21;
        matrix3x4.m22 = matrix4x4.m22;

        //xyz translation
        matrix3x4.m30 = -matrix4x4.m03 * 100;
        matrix3x4.m31 = matrix4x4.m13 * 100;
        matrix3x4.m32 = matrix4x4.m23 * 100;

        return matrix3x4;
    }

    //public static void MatrixToTransform(this Transform transform, Matrix4x4 matrix)
    //{
    //    Vector3 forward;
    //    forward.x = matrix.m02;
    //    forward.y = matrix.m12;
    //    forward.z = matrix.m22;
    //
    //    Vector3 upwards;
    //    upwards.x = matrix.m01;
    //    upwards.y = matrix.m11;
    //    upwards.z = matrix.m21;
    //    Quaternion rotation = Quaternion.LookRotation(forward, upwards);
    //
    //
    //    Vector3 position;
    //    position.x = matrix.m03;
    //    position.y = matrix.m13;
    //    position.z = matrix.m23;
    //
    //    Vector3 scale;
    //    scale.x = new Vector4(matrix.m00, matrix.m10, matrix.m20, matrix.m30).magnitude;
    //    scale.y = new Vector4(matrix.m01, matrix.m11, matrix.m21, matrix.m31).magnitude;
    //    scale.z = new Vector4(matrix.m02, matrix.m12, matrix.m22, matrix.m32).magnitude;
    //
    //    transform.localScale = scale;
    //    transform.rotation = rotation;
    //    transform.position = position;
    //}

    public static void FromMatrix(this Transform transform, Matrix4x4 matrix, bool local)
    {
        if (local)
        {
            transform.localPosition = matrix.GetPosition();
            transform.localRotation = matrix.rotation;
            transform.localScale = matrix.ExtractScale();
        }
        else
        {
            transform.position = matrix.GetPosition();
            transform.rotation = matrix.rotation;
            Vector3 globalScale = matrix.ExtractScale();
            transform.localScale = Vector3.one;
            transform.localScale = new Vector3(globalScale.x / transform.lossyScale.x, globalScale.y / transform.lossyScale.y, globalScale.z / transform.lossyScale.z);
        }
    }

    public static Matrix4x4 ToMatrix(this Transform transform, bool local)
    {
        if(local)
        {
            return transform.worldToLocalMatrix;
        }
        else
        {
            return transform.localToWorldMatrix;
        }
    }

    //public static Quaternion ExtractRotation(this Matrix4x4 matrix)
    //{
    //    Vector3 forward;
    //    forward.x = matrix.m02;
    //    forward.y = matrix.m12;
    //    forward.z = matrix.m22;
    //    
    //    Vector3 upwards;
    //    upwards.x = matrix.m01;
    //    upwards.y = matrix.m11;
    //    upwards.z = matrix.m21;
    //    
    //    return Quaternion.LookRotation(forward, upwards);
    //
    //    // Adapted from: http://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToQuaternion/index.htm
    //    //Quaternion q = new Quaternion();
    //    //q.w = Mathf.Sqrt(Mathf.Max(0, 1 + matrix[0, 0] + matrix[1, 1] + matrix[2, 2])) / 2;
    //    //q.x = Mathf.Sqrt(Mathf.Max(0, 1 + matrix[0, 0] - matrix[1, 1] - matrix[2, 2])) / 2;
    //    //q.y = Mathf.Sqrt(Mathf.Max(0, 1 - matrix[0, 0] + matrix[1, 1] - matrix[2, 2])) / 2;
    //    //q.z = Mathf.Sqrt(Mathf.Max(0, 1 - matrix[0, 0] - matrix[1, 1] + matrix[2, 2])) / 2;
    //    //q.x *= Mathf.Sign(q.x * (matrix[2, 1] - matrix[1, 2]));
    //    //q.y *= Mathf.Sign(q.y * (matrix[0, 2] - matrix[2, 0]));
    //    //q.z *= Mathf.Sign(q.z * (matrix[1, 0] - matrix[0, 1]));
    //    //return q;
    //}
    //
    //public static Vector3 ExtractPosition(this Matrix4x4 matrix)
    //{
    //    Vector3 position;
    //    position.x = matrix.m03;
    //    position.y = matrix.m13;
    //    position.z = matrix.m23;
    //    return position;
    //}

    public static Vector3 ExtractScale(this Matrix4x4 matrix)
    {
        Vector3 scale;
        scale = matrix.lossyScale;
        //scale.x = -scale.x;
        scale.x = RoundAwayFrom(new Vector4(matrix.m00, matrix.m10, matrix.m20, matrix.m30).magnitude, 1, 0.001f);
        scale.y = RoundAwayFrom(new Vector4(matrix.m01, matrix.m11, matrix.m21, matrix.m31).magnitude, 1, 0.001f);
        scale.z = RoundAwayFrom(new Vector4(matrix.m02, matrix.m12, matrix.m22, matrix.m32).magnitude, 1, 0.001f);
        return scale;
    }

    public static float RoundAwayFrom(float value, float target, float distance)
    {
        if(value > target-distance && value < target + distance)
        {
            return target;
        }
        return value;
    }

    public static void VerticallyFlipRenderTexture(ref Texture2D texture)
    {
        RenderTexture renderTex = RenderTexture.GetTemporary(texture.width, texture.height, 0);
        Graphics.Blit(texture, renderTex, new Vector2(1, -1), new Vector2(0, 1));
        RenderTexture.active = renderTex;
        texture = new Texture2D(texture.width, texture.height, TextureFormat.ARGB32, false);
        //newTex.ReadPixels(new Rect(0, 0, texture.width, texture.height), 0, 0);
        Graphics.CopyTexture(renderTex, texture);
        //newTex.Apply();
        RenderTexture.ReleaseTemporary(renderTex);
    }
    public static IEnumerable<Transform> Children(this Transform t)
    {
        foreach (Transform c in t)
            yield return c;
    }

    public static object DeepCopy(object obj)
    {
        if (obj == null)
            return null;
        Type type = obj.GetType();

        if (type.IsValueType || type == typeof(string))
        {
            return obj;
        }
        else if (type.IsArray)
        {
            Type elementType = type.GetElementType();
            var array = obj as Array;
            Array copied = Array.CreateInstance(elementType, array.Length);
            for (int i = 0; i < array.Length; i++)
            {
                copied.SetValue(DeepCopy(array.GetValue(i)), i);
            }
            return Convert.ChangeType(copied, obj.GetType());
        }
        else if (type.IsClass)
        {

            object toret = Activator.CreateInstance(obj.GetType());
            FieldInfo[] fields = type.GetFields(BindingFlags.Public |
                        BindingFlags.NonPublic | BindingFlags.Instance);
            foreach (FieldInfo field in fields)
            {
                object fieldValue = field.GetValue(obj);
                if (fieldValue == null)
                    continue;
                field.SetValue(toret, DeepCopy(fieldValue));
            }
            return toret;
        }
        else
            throw new ArgumentException("Unknown type");
    }

    public static T[,] Resize2D<T>(this T[,] original, int m, int n)
    {

        T[,] newArray = new T[m, n];
        int mMin = Math.Min(original.GetLength(0), newArray.GetLength(0));
        int nMin = Math.Min(original.GetLength(1), newArray.GetLength(1));

        for (int i = 0; i < mMin; i++)
            Array.Copy(original, i * original.GetLength(1), newArray, i * newArray.GetLength(1), nMin);

        return newArray;
    }

    public static Texture2D MakeBackgroundTexture(int width, int height, Color color)
    {
        Color[] pixels = new Color[width * height];

        for (int i = 0; i < pixels.Length; i++)
        {
            pixels[i] = color;
        }

        Texture2D backgroundTexture = new Texture2D(width, height);

        backgroundTexture.SetPixels(pixels);
        backgroundTexture.Apply();

        return backgroundTexture;
    }
}
