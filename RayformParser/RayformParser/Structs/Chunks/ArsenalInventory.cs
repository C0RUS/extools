﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Rayform
{
    public class ArsenalInventory : Chunk
    {

        public Tab[] weapons = new Tab[5];
        public Tab[] shields = new Tab[5];
        public Tab[] armour = new Tab[5];
        public Tab[] clothing = new Tab[5];

        public class Tab
        {
            public List<InventoryItem> items = new List<InventoryItem>();
        }
    }
}
