﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Rayform
{
    public class Exploration : Chunk
    {
        public List<Id> readBooks = new List<Id>(); // The IDs of published books read by the player. They give XP, so this makes sure you can't keep reading them.
        public List<Locale> locales = new List<Locale>();

        public class Locale : Struct
        {
            public Str128 name; // The name of a locale, probably either points to an entry in locales.rdb or the locale's file name without the .rfc extension.
            public int TilesWidth; // Equal to GridExt in the locale's RFC file.
            public int TilesHeight; // Equal to GridExt2.
            public List<byte> TilesExplored = new List<byte>(); //TilesExplored is an array of booleans equal in amount to the tiles in the locale. The booleans' ordering in relation to the tiles in the RFC has not been discerned yet.

            public override void ReadStruct(Stream fs)
            {
                name = Parser.ReadStr128(fs);
                TilesWidth = Parser.ReadInt32(fs);
                TilesHeight = Parser.ReadInt32(fs);

                for (int i = 0; i < TilesWidth * TilesHeight/8; i++)
                {
                    TilesExplored.Add(Parser.ReadInt8(fs));
                }
            }

            public override void WriteStruct(Stream fs)
            {
                Parser.WriteStr128(fs, name);
                Parser.WriteInt32(fs, TilesWidth);
                Parser.WriteInt32(fs, TilesHeight);

                foreach (byte tile in TilesExplored )
                {
                    Parser.WriteInt8(fs, tile);
                }
            }
        }

        public override void ReadStruct(Stream fs)
        {
            int readBooks_n = Parser.ReadInt32(fs);
            for (int i = 0; i < readBooks_n; i++)
            {
                Id bookId = new Id();
                bookId.ReadStruct(fs);
                readBooks.Add(bookId);
            }

            int locales_n = Parser.ReadInt32(fs);
            for (int i = 0; i < locales_n; i++)
            {
                Locale locale = new Locale();
                locale.ReadStruct(fs);
                locales.Add(locale);
            }
        }

        public override void WriteStruct(Stream fs)
        {
            Parser.WriteInt32(fs, readBooks.Count);
            foreach (Id bookId in readBooks)
            {
                bookId.WriteStruct(fs);
            }

            Parser.WriteInt32(fs, locales.Count);
            foreach (Locale locale in locales)
            {
                locale.WriteStruct(fs);
            }
        }
    }
}
