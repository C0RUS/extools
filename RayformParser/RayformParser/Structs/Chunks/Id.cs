﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rayform
{
    [Serializable]
    public struct Id
    {
        public enum Flags : ushort
        {
            Global = 0, // If no flag is present, the id is understood to be a global object, from Resource.rpk/objdb.rdb.
			World = 0x4000, // The id is a world item, from the save's '#worldobjdb' section.
			Local = 0x8000 // The id is a local item, from the locale's objdb, or actor's objdb in actors.rpk.
		}

        public ushort baseId;
        public Flags flags; 

        public void ReadStruct(Stream fs)
        {
            baseId = Parser.ReadUInt16(fs);
            flags = (Flags)Parser.ReadUInt16(fs);
        }

		public void WriteStruct(Stream fs)
		{
            Parser.WriteUInt16(fs, baseId);
            Parser.WriteUInt16(fs, (ushort)flags);
		}
	}
}