﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Rayform.ArsenalInventory;

namespace Rayform
{
    public class ManagerItems : Chunk //Arsenal weapons and apparel that the manager started with.Sold items are removed from here.It's not clear what this is used for, if anything.
    {
        //[FieldSize(typeof(int))]
        public List<InventoryItem> items = new List<InventoryItem>();
    }
}
