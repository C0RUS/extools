﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using static Rayform.CharacterPosition;

namespace Rayform
{
    public class Material : Chunk
    {
        public Str128 name;
        public int unknown0;
        public int unknown1;
        public int unknown2;
        public int unknown3;
        public float unknown4;
        public float unknown5;
        public float unknown6;
        public float unknown7;
        public float unknown8;
        public float unknown9;
        public float unknown10;
        public float unknown11;
        public float unknown12;
        public float unknown13;
        public float unknown14;
        public float unknown15;
        public float unknown16;
        public float unknown17;
        public float unknown18;
        public float unknown19;
        public float unknown20;
        public float unknown21;
        public float unknown22;
        public float unknown23;
        public float unknown24;
        public float unknown25;
        public float unknown26;
        public float unknown27;
        public float unknown28;
        public float unknown29;
        public float unknown30;
        public float unknown31;
        public float unknown32;
        public float unknown33;
        public float unknown34;
        public float unknown35;
        public float unknown36;
        public float unknown37;
        public float unknown38;
        public float unknown39;
        public float unknown40;
        public float unknown41;
        public float unknown42;
        public float unknown43;
        public float unknown44;
        public float unknown45;
        public Texture diffuse;
        public Texture diffuse2;
        public Texture bump;
        public Texture heightmap;
        public Texture specular;
        public Texture specular2;
        public Texture roughness;

        public override void ReadStruct(Stream fs)
        {
            name = Parser.ReadStr128(fs);
            ReadMaterial(fs);
        }

        public void ReadStruct(Stream fs, string name, long size)
        {
            this.chunkSize = size;
            start = fs.Position;
            this.name = name;
            ReadMaterial(fs);
        }

        public void ReadMaterial(Stream fs)
        {
            //start = fs.Position;
            //name = Parser.ReadStr128(fs);
            unknown0 = Parser.ReadInt32(fs);
            unknown1 = Parser.ReadInt32(fs);
            unknown2 = Parser.ReadInt32(fs);
            unknown3 = Parser.ReadInt32(fs);
            unknown4 = Parser.ReadFloat(fs);
            unknown5 = Parser.ReadFloat(fs);
            unknown6 = Parser.ReadFloat(fs);
            unknown7 = Parser.ReadFloat(fs);
            unknown8 = Parser.ReadFloat(fs);
            unknown9 = Parser.ReadFloat(fs);
            unknown10 = Parser.ReadFloat(fs);
            unknown11 = Parser.ReadFloat(fs);
            unknown12 = Parser.ReadFloat(fs);
            unknown13 = Parser.ReadFloat(fs);
            unknown14 = Parser.ReadFloat(fs);
            unknown15 = Parser.ReadFloat(fs);
            unknown16 = Parser.ReadFloat(fs);
            unknown17 = Parser.ReadFloat(fs);
            unknown18 = Parser.ReadFloat(fs);
            unknown19 = Parser.ReadFloat(fs);
            unknown20 = Parser.ReadFloat(fs);
            unknown21 = Parser.ReadFloat(fs);
            unknown22 = Parser.ReadFloat(fs);
            unknown23 = Parser.ReadFloat(fs);
            unknown24 = Parser.ReadFloat(fs);
            unknown25 = Parser.ReadFloat(fs);
            unknown26 = Parser.ReadFloat(fs);
            unknown27 = Parser.ReadFloat(fs);
            unknown28 = Parser.ReadFloat(fs);
            unknown29 = Parser.ReadFloat(fs);
            unknown30 = Parser.ReadFloat(fs);
            unknown31 = Parser.ReadFloat(fs);
            unknown32 = Parser.ReadFloat(fs);
            unknown33 = Parser.ReadFloat(fs);
            unknown34 = Parser.ReadFloat(fs);
            unknown35 = Parser.ReadFloat(fs);
            unknown36 = Parser.ReadFloat(fs);
            unknown37 = Parser.ReadFloat(fs);
            unknown38 = Parser.ReadFloat(fs);
            unknown39 = Parser.ReadFloat(fs);
            unknown40 = Parser.ReadFloat(fs);
            unknown41 = Parser.ReadFloat(fs);
            unknown42 = Parser.ReadFloat(fs);
            unknown43 = Parser.ReadFloat(fs);
            unknown44 = Parser.ReadFloat(fs);
            unknown45 = Parser.ReadFloat(fs);

            Debug.WriteLine(fs.Position);
            // Textures use a structure similar to heavychunks
            while(fs.Position < end)
            {
                Texture texture = new Texture();
                texture.ReadStruct(fs);
                if (texture.type == Utils.HexStringtoInt32("00 00 02 00"))
                {
                    diffuse = texture;
                }
                else if (texture.type == Utils.HexStringtoInt32("00 00 40 00"))
                {
                    diffuse2 = texture;
                }
                else if (texture.type == Utils.HexStringtoInt32("00 00 20 00"))
                {
                    bump = texture;
                }
                else if (texture.type == Utils.HexStringtoInt32("00 00 80 00"))
                {
                    heightmap = texture;
                }
                else if (texture.type == Utils.HexStringtoInt32("00 00 04 00"))
                {
                    specular = texture;
                }
                else if (texture.type == Utils.HexStringtoInt32("00 00 00 01"))
                {
                    specular2 = texture;
                }
                else if (texture.type == Utils.HexStringtoInt32("00 00 10 00"))
                {
                    roughness = texture;
                }
            }

            //Debug.WriteLine(fs.Position - start);
        }
        public class Texture : Struct
        {
            public int type;
            public int size;
            public float unknown0;
            public float unknown1;
            public float unknown2;
            public float unknown3;
            public float unknown4;
            public float unknown5;
            public float unknown6;
            public float unknown7;
            public Str128 name;

            public override void ReadStruct(Stream fs)
            {
                type = Parser.ReadInt32(fs);
                size = Parser.ReadInt32(fs);
                unknown0 = Parser.ReadInt32(fs);
                unknown1 = Parser.ReadInt32(fs);
                unknown2 = Parser.ReadInt32(fs);
                unknown3 = Parser.ReadInt32(fs);
                unknown4 = Parser.ReadFloat(fs);
                unknown5 = Parser.ReadFloat(fs);
                unknown6 = Parser.ReadFloat(fs);
                unknown7 = Parser.ReadFloat(fs);
                name = Parser.ReadStr128(fs);
            }
        }


        public override void WriteStruct(Stream fs)
		{
			Parser.WriteStr128(fs, name);
			Parser.WriteInt32(fs, unknown0);
			Parser.WriteInt32(fs, unknown1);
			Parser.WriteInt32(fs, unknown2);
			Parser.WriteInt32(fs, unknown3);
			Parser.WriteFloat(fs, unknown4);
			Parser.WriteFloat(fs, unknown5);
			Parser.WriteFloat(fs, unknown6);
			Parser.WriteFloat(fs, unknown7);
			Parser.WriteFloat(fs, unknown8);
			Parser.WriteFloat(fs, unknown9);
			Parser.WriteFloat(fs, unknown10);
			Parser.WriteFloat(fs, unknown11);
			Parser.WriteFloat(fs, unknown12);
			Parser.WriteFloat(fs, unknown13);
			Parser.WriteFloat(fs, unknown14);
			Parser.WriteFloat(fs, unknown15);
			Parser.WriteFloat(fs, unknown16);
			Parser.WriteFloat(fs, unknown17);
			Parser.WriteFloat(fs, unknown18);
			Parser.WriteFloat(fs, unknown19);
			Parser.WriteFloat(fs, unknown20);
			Parser.WriteFloat(fs, unknown21);
			Parser.WriteFloat(fs, unknown22);
			Parser.WriteFloat(fs, unknown23);
			Parser.WriteFloat(fs, unknown24);
			Parser.WriteFloat(fs, unknown25);
			Parser.WriteFloat(fs, unknown26);
			Parser.WriteFloat(fs, unknown27);
			Parser.WriteFloat(fs, unknown28);
			Parser.WriteFloat(fs, unknown29);
			Parser.WriteFloat(fs, unknown30);
			Parser.WriteFloat(fs, unknown31);
			Parser.WriteFloat(fs, unknown32);
			Parser.WriteFloat(fs, unknown33);
			Parser.WriteFloat(fs, unknown34);
			Parser.WriteFloat(fs, unknown35);
			Parser.WriteFloat(fs, unknown36);
			Parser.WriteFloat(fs, unknown37);
			Parser.WriteFloat(fs, unknown38);
			Parser.WriteFloat(fs, unknown39);
			Parser.WriteFloat(fs, unknown40);
			Parser.WriteFloat(fs, unknown41);
			Parser.WriteFloat(fs, unknown42);
			Parser.WriteFloat(fs, unknown43);
            Parser.WriteFloat(fs, unknown44);
			Parser.WriteFloat(fs, unknown45);
            WriteTexture(fs, diffuse, "00 00 02 00");
            WriteTexture(fs, diffuse2, "00 00 40 00");
            WriteTexture(fs, bump, "00 00 20 00");
            WriteTexture(fs, heightmap, "00 00 80 00");
            WriteTexture(fs, specular, "00 00 04 00");
            WriteTexture(fs, specular2, "00 00 00 01");
            WriteTexture(fs, roughness, "00 00 10 00");
        }

        private void WriteTexture(Stream fs, Texture texture, string typeHex)
        {
            if (texture != null)
            {
                Parser.WriteInt32(fs, Utils.HexStringtoInt32(typeHex));
                Parser.WriteInt32(fs, texture.size);
                Parser.WriteInt32(fs, (int)texture.unknown0);
                Parser.WriteInt32(fs, (int)texture.unknown1);
                Parser.WriteInt32(fs, (int)texture.unknown2);
                Parser.WriteInt32(fs, (int)texture.unknown3);
                Parser.WriteFloat(fs, texture.unknown4);
                Parser.WriteFloat(fs, texture.unknown5);
                Parser.WriteFloat(fs, texture.unknown6);
                Parser.WriteFloat(fs, texture.unknown7);
                Parser.WriteStr128(fs, texture.name);
            }
        }
    }
}
