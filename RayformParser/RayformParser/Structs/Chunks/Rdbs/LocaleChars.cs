﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Rayform
{
    public class LocaleChars : Chunk // Chunk that contains a list of char rdbs and positions, from locales
    {
        public List<Chunk> chunks = new List<Chunk>();

        public override void ReadStruct(Stream fs)
        {
            int chunks_n = Parser.ReadInt32(fs)+1;
            for (int i = 0; i < chunks_n; i++)
            {
                Chunk chunk = StartReadChunk(fs);
                chunk.ReadStruct(fs);
                chunk.EndReadChunk(fs);
                chunks.Add(chunk);
            }
        }

        public override void WriteStruct(Stream fs)
        {
            Parser.WriteInt32(fs, chunks.Count-1);
            foreach(Chunk chunk in chunks)
            {
                chunk.WriteChunk(fs);
            }
        }
    }
}