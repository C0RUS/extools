﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Rayform
{
    public class LocaleObjs : Chunk // Chunk that contains a list of obj rdbs, from locales
    {
        public List<Chunk> chunks = new List<Chunk>();

        public override void ReadStruct(Stream fs)
        {
            while(fs.Position < end)
            {
                Chunk rdb = Chunk.StartReadChunk(fs);
                rdb.ReadStruct(fs);
                rdb.EndReadChunk(fs);
                chunks.Add(rdb);
            }
        }

        public override void WriteStruct(Stream fs)
        {
            foreach(Chunk chunk in chunks)
            {
                chunk.WriteChunk(fs);
            }
        }
    }
}
