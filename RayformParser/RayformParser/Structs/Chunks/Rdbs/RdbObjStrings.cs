﻿using Newtonsoft.Json;
using Rayform;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;

namespace Rayform
{
    public class RdbObjStrings : Rdb
    {
        public override object ReadRdbData(Stream fs, uint size)
        {
            return Parser.ReadString(fs, (int)size);
        }

        public override void WriteRdbData(Stream fs, object data)
        {
            Parser.WriteString(fs, (string)data);
        }

        // The struct used for rdb indexing
        //public new class Entry : Rdb.Entry
        //{
        //    public new string data;
        //}
    }
}