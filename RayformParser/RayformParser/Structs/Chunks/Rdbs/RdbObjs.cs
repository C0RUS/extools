﻿using Newtonsoft.Json;
using Rayform;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;

namespace Rayform
{
    public class RdbObjs : Rdb
    {
        //public new List<Entry> entries = new List<Entry>();
        public override object ReadRdbData(Stream fs, uint size)
        {
            ObjClass obj = new ObjClass();
            obj.ReadStruct(fs, size);
            return obj;
        }

        public override void WriteRdbData(Stream fs, object data)
        {
            ((ObjClass)data).WriteStruct(fs);
        }

        // The struct used for rdb indexing
        //public new class Entry : Rdb.Entry
        //{
        //    public new ObjClass data;
        //}
    }
}