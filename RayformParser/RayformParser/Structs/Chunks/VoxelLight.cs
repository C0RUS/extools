﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Rayform
{
    public class VoxelLight : Scene.Node
    {
        public int unknown1;
        public Color color;
        public float multiplier;
        public float attenDist;
        public int unknown3;

        public override void ReadNode(Stream fs)
        {
            unknown1 = Parser.ReadInt32(fs);
            color = Parser.ReadColor(fs);
            multiplier = Parser.ReadFloat(fs);
            attenDist = Parser.ReadFloat(fs);
            unknown3 = Parser.ReadInt32(fs);
        }
        public override void WriteNode(Stream fs)
        {
            Parser.WriteInt32(fs, unknown1);
            Parser.WriteColor(fs, color);
            Parser.WriteFloat(fs, multiplier);
            Parser.WriteFloat(fs, attenDist);
            Parser.WriteInt32(fs, unknown3);
        }

    }
}
