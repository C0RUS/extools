﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Rayform
{
    public class Playerdata : Struct
    {
        public int version;
        public Str128 locale; // The locale the player is currently in.
        Vector3 cameraPosition;
        public float zoom;
        public byte copper; // Arena money. Doesn't exceed 11 in-game, as 12 makes a silver.
        public byte silver; // Ditto, with 12 making a gold.
        public ushort gold; // Ditto. Has no overflow protection, wraps around with 65535+1 resulting in 0. Don't be too successful.
        public int unknown0;
        public int unknown1;
        public int unknown2;
        public List<Chunk> chunks = new List<Chunk>();

        public void ReadStruct(Stream fs, long size)
        {
            long start = fs.Position;
            version = Parser.ReadInt32(fs);
            locale = Parser.ReadStr128(fs);
            cameraPosition = Parser.ReadVector3(fs);
            zoom = Parser.ReadFloat(fs);
            copper = Parser.ReadInt8(fs);
            silver = Parser.ReadInt8(fs);
            gold = Parser.ReadUInt16(fs);
            unknown0 = Parser.ReadInt32(fs);
            unknown1 = Parser.ReadInt32(fs);
            unknown2 = Parser.ReadInt32(fs);

            while(fs.Position < start + size)
            { 
                Chunk chunk = Chunk.StartReadChunk(fs);
                chunk.ReadStruct(fs);
                chunk.EndReadChunk(fs);
                chunks.Add(chunk);
            }
        }

        public override void WriteStruct(Stream fs)
        {
            Parser.WriteInt32(fs, version);
            Parser.WriteStr128(fs, locale);
            Parser.WriteVector3(fs, cameraPosition);
            Parser.WriteFloat(fs, zoom);
            Parser.WriteInt8(fs, copper);
            Parser.WriteInt8(fs, silver);
            Parser.WriteUInt16(fs, gold);
            Parser.WriteInt32(fs, unknown0);
            Parser.WriteInt32(fs, unknown1);
            Parser.WriteInt32(fs, unknown2);

            foreach (Chunk chunk in chunks)
            {
                chunk.WriteChunk(fs);
            }
        }
    }
}