﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Rayform
{
	public class Container : HeavyChunk //20 00 00 00
	{
		public ushort width;
		public ushort height;
		public Id keyId = new Id(); // Maybe (???)
		public ushort capacity;
		public ushort maxSize;
		public int unknown3;
		public int unknown4;
		public List<InventoryItem> items = new List<InventoryItem>(); //items in the inventory

        public override void ReadStruct(Stream fs)
        {
			width = Parser.ReadUInt16(fs);
			height = Parser.ReadUInt16(fs);
			keyId.ReadStruct(fs);
			capacity = Parser.ReadUInt16(fs);
			maxSize = Parser.ReadUInt16(fs);
			unknown3 = Parser.ReadInt32(fs);
			unknown4 = Parser.ReadInt32(fs);

			int items_n = Parser.ReadInt32(fs);
			for(int i=0; i<items_n; i++)
			{
				InventoryItem item = new InventoryItem();
				item.ReadStruct(fs);
				items.Add(item);
			}
        }

        public override void WriteStruct(Stream fs)
        {
            Parser.WriteUInt16(fs, width);
            Parser.WriteUInt16(fs, height);
            keyId.WriteStruct(fs);
            Parser.WriteUInt16(fs, capacity);
            Parser.WriteUInt16(fs, maxSize);
            Parser.WriteInt32(fs, unknown3);
            Parser.WriteInt32(fs, unknown4);

            Parser.WriteInt32(fs, items.Count);
            foreach(InventoryItem item in items)
            {
                item.WriteStruct(fs);
            }
        }
    }
}
