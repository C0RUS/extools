﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Rayform
{
	public class Door : HeavyChunk //00 00 04 00
    {
        public DoorFlag doorFlags;
        public byte unk1;
        public byte unk2;
		public byte unk3;
		public Id keyId = new Id();
		public int unknown1;
		public int unknown2;
		public int unknown3;

        public enum DoorFlag : byte
        {
            disableOpen = 0x10,
            disableClose  = 0x30,
            leverReset  = 0x50,
            inswing = 0x80,

            unk1 = 0x01,
            unk2 = 0x02,
            unk3 = 0x03,
            seeThrough = 0x04,
            uninteractable = 0x08,
        }

        public override void ReadStruct(Stream fs)
		{
            doorFlags = (DoorFlag)Parser.ReadInt8(fs);
            unk1 = Parser.ReadInt8(fs);
			unk2 = Parser.ReadInt8(fs);
			unk3 = Parser.ReadInt8(fs);
            keyId.ReadStruct(fs);
            unknown1 = Parser.ReadInt32(fs);
			unknown2 = Parser.ReadInt32(fs);
            unknown3 = Parser.ReadInt32(fs);
        }

        public override void WriteStruct(Stream fs)
        {
            Parser.WriteInt8(fs, (byte)doorFlags);
            Parser.WriteInt8(fs, unk1);
            Parser.WriteInt8(fs, unk2);
            Parser.WriteInt8(fs, unk3);
            keyId.WriteStruct(fs);
            Parser.WriteInt32(fs, unknown1);
            Parser.WriteInt32(fs, unknown2);
            Parser.WriteInt32(fs, unknown3);
        }
    }
}
