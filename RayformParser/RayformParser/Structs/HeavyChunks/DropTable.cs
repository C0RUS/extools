﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Rayform
{
    public class DropTable : HeavyChunk
    {
        public int unknown1;
        public byte SpclItems;
        public byte SpclItems2;
        public short unknown3;
        public int unknown4;
        public List<Item> items = new List<Item>();

        public override void ReadStruct(Stream fs)
        {
            unknown1 = Parser.ReadInt32(fs);
            SpclItems = Parser.ReadInt8(fs);
            SpclItems2 = Parser.ReadInt8(fs);
            unknown3 = Parser.ReadInt16(fs);
            unknown4 = Parser.ReadInt32(fs);

            int items_n = Parser.ReadInt32(fs);
            for (int i = 0; i < items_n; i++)
            {
                Item item = new Item();
                item.ReadStruct(fs);
                items.Add(item);
            }
        }

        public override void WriteStruct(Stream fs)
        {
            Parser.WriteInt32(fs, unknown1);
            Parser.WriteInt8(fs, SpclItems);
            Parser.WriteInt8(fs, SpclItems2);
            Parser.WriteInt16(fs, unknown3);
            Parser.WriteInt32(fs, unknown4);

            Parser.WriteInt32(fs, items.Count);
            foreach (Item item in items)
            {
                item.WriteStruct(fs);
            }
        }

        public class Item : Struct
        {
            public Id id = new Id();
            public byte chance;
            public byte rarity;
            public byte min;
            public byte max;
            public SpawnFlags spawnFlags;
            public byte SpawnCount;
            public byte wearVariance;
            public byte dirtVariance;
            public byte unknown1;
            public byte unknown2;
            public byte unknown3;
            public byte unknown4;

            public enum SpawnFlags : byte //Needs to be a bitmask, this is a crappy temporary solution
            {
                None = 0,
                Unknown1 = 2,
                Special = 128,
                Unknown2 = 192
            }

            public override void ReadStruct(Stream fs)
            {
                id.ReadStruct(fs);
                chance = Parser.ReadInt8(fs);
                rarity = Parser.ReadInt8(fs);
                min = Parser.ReadInt8(fs);
                max = Parser.ReadInt8(fs);
                spawnFlags = (SpawnFlags)Parser.ReadInt8(fs);
                SpawnCount = Parser.ReadInt8(fs);
                wearVariance = Parser.ReadInt8(fs);
                dirtVariance = Parser.ReadInt8(fs);
                unknown1 = Parser.ReadInt8(fs);
                unknown2 = Parser.ReadInt8(fs);
                unknown3 = Parser.ReadInt8(fs);
                unknown4 = Parser.ReadInt8(fs);
            }

            public override void WriteStruct(Stream fs)
            {
                id.WriteStruct(fs);
                Parser.WriteInt8(fs, chance);
                Parser.WriteInt8(fs, rarity);
                Parser.WriteInt8(fs, min);
                Parser.WriteInt8(fs, max);
                Parser.WriteInt8(fs, (byte)spawnFlags);
                Parser.WriteInt8(fs, SpawnCount);
                Parser.WriteInt8(fs, wearVariance);
                Parser.WriteInt8(fs, dirtVariance);
                Parser.WriteInt8(fs, unknown1);
                Parser.WriteInt8(fs, unknown2);
                Parser.WriteInt8(fs, unknown3);
                Parser.WriteInt8(fs, unknown4);
            }
        }
    }
}