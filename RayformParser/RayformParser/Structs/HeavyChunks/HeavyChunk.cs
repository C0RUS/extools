﻿using Newtonsoft.Json;
using Rayform.Chunks;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Rayform
{
	// A heavy chunk is a chunk which counts the 8 bytes (the version) as part of its size.
	public class HeavyChunk : Struct
	{
		[JsonIgnore]
		public long start;
        [JsonIgnore]
        public long chunkSize;
        [JsonIgnore]
        public long end { get { return start + chunkSize - 8; } }

		public static Dictionary<int, Type> heavyChunkDic =
		new Dictionary<int, Type>() {
			{ Utils.HexStringtoInt32("00 00 00 10"), typeof(Thing) },
			{ Utils.HexStringtoInt32("00 00 10 00"), typeof(Object) },
			{ Utils.HexStringtoInt32("00 00 04 00"), typeof(Door) },
			{ Utils.HexStringtoInt32("00 00 02 00"), typeof(Container) },
			{ Utils.HexStringtoInt32("00 00 01 00"), typeof(Operative) },
			{ Utils.HexStringtoInt32("00 80 00 00"), typeof(Shield) },
			{ Utils.HexStringtoInt32("00 40 00 00"), typeof(Weapon) },
			{ Utils.HexStringtoInt32("00 10 00 00"), typeof(Apparel) },
            { Utils.HexStringtoInt32("00 02 00 00"), typeof(DropTable) },
            { Utils.HexStringtoInt32("07 AC 00 80"), typeof(Trigger) },
            { Utils.HexStringtoInt32("03 0B 00 80"), typeof(Map) },
			//{ Utils.HexStringtoInt32("00 01 00 00"), typeof(Torch) },
        };
        //[JsonIgnore]
        public int chunkId;

		public static HeavyChunk StartReadHeavyChunk(Stream fs)
		{
			HeavyChunk heavyChunk;
			int chunkId;
			uint chunkSize;
			long start = fs.Position;
			chunkId = Parser.ReadInt32(fs);
			chunkSize = Parser.ReadUInt32(fs);

			Type type;
			heavyChunkDic.TryGetValue(chunkId, out type);

			if (type == null)
			{
				Console.WriteLine("ERROR: NO HEAVY CHUNK FOUND OF ID " + Utils.BytesToHexString(BitConverter.GetBytes(chunkId)));
				heavyChunk = new UnknownHeavyChunk();
				heavyChunk.chunkSize = chunkSize;
				heavyChunk.start = fs.Position;
				heavyChunk.chunkId = chunkId;
				return heavyChunk;
			}
			else
			{
				Console.WriteLine("Heavy Chunk found - ID: " + Utils.BytesToHexString(BitConverter.GetBytes(chunkId)) + " Size: " + chunkSize + " Type: " + type);
				heavyChunk = Activator.CreateInstance(type, null) as HeavyChunk;
				heavyChunk.chunkSize = chunkSize;
				heavyChunk.start = fs.Position;
				heavyChunk.chunkId = chunkId;
				return heavyChunk;
			}
		}

		public void EndReadHeavyChunk(Stream fs)
		{
			fs.Position = end;
		}

		public void WriteHeavyChunk(Stream fs)
		{
            StartWriteHeavyChunk(fs);
            WriteStruct(fs);
            EndWriteHeavyChunk(fs);
        }

        public void StartWriteHeavyChunk(Stream fs)
        {
            fs.Position += 8;
            start = fs.Position;
        }

        public void EndWriteHeavyChunk(Stream fs)
        {
			chunkSize = fs.Position - start + 8;

            // Go back and write the chunk header
			fs.Position = start - 8;
            int chunkId = heavyChunkDic.FirstOrDefault(x => x.Value == GetType()).Key;
            if (chunkId != 0)
			{
				this.chunkId = chunkId;
			}  
            Parser.WriteInt32(fs, this.chunkId);
            Parser.WriteUInt32(fs, (uint)chunkSize);

            // Return
            fs.Position = start + chunkSize - 8;
        }

        public override void ReadStruct(Stream fs)
		{
			Console.WriteLine("read not supported yet");
			fs.Position += chunkSize;
		}

		public override void WriteStruct(Stream fs)
		{
			Console.WriteLine("write not supported yet");
		}
	}
}
