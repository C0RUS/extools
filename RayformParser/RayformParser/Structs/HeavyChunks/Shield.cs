﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using static Rayform.Apparel;
using static Rayform.CharacterPosition;
using static Rayform.Weapon;

namespace Rayform
{
	public class Shield : HeavyChunk // 00 80 00 00
	{
		public byte unknown1;
		public byte unknown2;
		public byte unknown3;
		public Flags flags;
        public byte weight;
		public byte impactDefense;
		public byte encumbrance;
		public byte unknown8;
		public byte unknown9;
		public byte unknown10;
		public byte unknown11;
		public byte unknown12;

		public int unknown13;
		public int unknown14;
		public int unknown15;
		public int unknown16;
		public int unknown17;
		public int unknown18;
		public int unknown19;
		public int unknown20;
        public int unknown21;
        public int unknown22;
        public int unknown23;
        public int unknown24;
        public int unknown25;
        public int unknown26;
        public int unknown27;
        public int unknown28;
        public int unknown29;

        public enum Flags
        {
            None = 0,
            HideStats = 1,
            Giant = 2
        }

        public override void ReadStruct(Stream fs)
        {
            unknown1 = Parser.ReadInt8(fs);
            unknown2 = Parser.ReadInt8(fs);
            unknown3 = Parser.ReadInt8(fs);
            flags = (Flags)Parser.ReadInt8(fs);
            weight = Parser.ReadInt8(fs);
            impactDefense = Parser.ReadInt8(fs);
            encumbrance = Parser.ReadInt8(fs);
            unknown8 = Parser.ReadInt8(fs);
            unknown9 = Parser.ReadInt8(fs);
            unknown10 = Parser.ReadInt8(fs);
            unknown11 = Parser.ReadInt8(fs);
            unknown12 = Parser.ReadInt8(fs);

            unknown13 = Parser.ReadInt32(fs);
            unknown14 = Parser.ReadInt32(fs);
            unknown15 = Parser.ReadInt32(fs);
            unknown16 = Parser.ReadInt32(fs);
            unknown17 = Parser.ReadInt32(fs);
            unknown18 = Parser.ReadInt32(fs);
            unknown19 = Parser.ReadInt32(fs);
            unknown20 = Parser.ReadInt32(fs);
            unknown21 = Parser.ReadInt32(fs);
            unknown22 = Parser.ReadInt32(fs);
            unknown23 = Parser.ReadInt32(fs);
            unknown24 = Parser.ReadInt32(fs);
            unknown25 = Parser.ReadInt32(fs);
            unknown26 = Parser.ReadInt32(fs);
            unknown27 = Parser.ReadInt32(fs);
            unknown28 = Parser.ReadInt32(fs);
            unknown29 = Parser.ReadInt32(fs);
        }

        public override void WriteStruct(Stream fs)
        {
            Parser.WriteInt8(fs, unknown1);
            Parser.WriteInt8(fs, unknown2);
            Parser.WriteInt8(fs, unknown3);
            Parser.WriteInt8(fs, (byte)flags);
            Parser.WriteInt8(fs, weight);
            Parser.WriteInt8(fs, impactDefense);
            Parser.WriteInt8(fs, encumbrance);
            Parser.WriteInt8(fs, unknown8);
            Parser.WriteInt8(fs, unknown9);
            Parser.WriteInt8(fs, unknown10);
            Parser.WriteInt8(fs, unknown11);
            Parser.WriteInt8(fs, unknown12);

            Parser.WriteInt32(fs, unknown13);
            Parser.WriteInt32(fs, unknown14);
            Parser.WriteInt32(fs, unknown15);
            Parser.WriteInt32(fs, unknown16);
            Parser.WriteInt32(fs, unknown17);
            Parser.WriteInt32(fs, unknown18);
            Parser.WriteInt32(fs, unknown19);
            Parser.WriteInt32(fs, unknown20);
            Parser.WriteInt32(fs, unknown21);
            Parser.WriteInt32(fs, unknown22);
            Parser.WriteInt32(fs, unknown23);
            Parser.WriteInt32(fs, unknown24);
            Parser.WriteInt32(fs, unknown25);
            Parser.WriteInt32(fs, unknown26);
            Parser.WriteInt32(fs, unknown27);
            Parser.WriteInt32(fs, unknown28);
            Parser.WriteInt32(fs, unknown29);
        }
    }
}
