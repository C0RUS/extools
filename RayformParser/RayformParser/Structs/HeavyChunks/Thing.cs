﻿using System.IO;

namespace Rayform
{
	public class Thing : HeavyChunk //00 00 00 10
	{
		public Str128 thingName;
		public int flags; //0x2 = Object definition is deprecated/unpublished, may be replaced. 0x1 = Singular, multiple instances of this object not intended.
		public Id parentId = new Id(); //The ID of another object definition to inherit properties from.

		public override void ReadStruct(Stream fs)
		{
			thingName = Parser.ReadStr128(fs);
			flags = Parser.ReadInt32(fs);
            parentId.ReadStruct(fs);
		}

        public override void WriteStruct(Stream fs)
        {
			Parser.WriteStr128(fs, thingName);
			Parser.WriteInt32(fs, flags);
			parentId.WriteStruct(fs);
        }
    }
}
