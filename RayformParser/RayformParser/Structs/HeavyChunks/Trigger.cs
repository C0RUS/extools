﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using static Rayform.CharacterPosition;
using static Rayform.Trigger;

namespace Rayform
{
    public class Trigger : HeavyChunk
    {
        public ushort sizeX;
        public ushort sizeY;
        public ushort sizeZ;
        public short unknown1;
        public Function function;
        public TriggerType trigger;
        public byte unknown2;  // One of these may be the shape type?
        public byte unknown3;
        public int unknown4;
        public Id funcId = new Id(); // Multi purpose ID used by the trigger. Could refer to a locale ID if used in locale transitions, or an object ID if used in alarms
        public string code = "";

        public enum Function : byte
        {
            None = 0,
            Alarm = 2, // Used to trigger golems
            RunScript = 16,
            LocaleTransition = 32,
            Teleport = 33,
            Entrance = 64 // Entrance from another locale? (I will get back to it, how I assume it works is that if you enter from another locale, you will spawn at the zone with the ID of the previous locale)
        }

        public enum TriggerType : byte
        {
            None = 0,
            Player = 16, // ???
            Character = 17,
            Narrator = 18

        }

        public override void ReadStruct(Stream fs)
        {
            sizeX = Parser.ReadUInt16(fs);
            sizeY = Parser.ReadUInt16(fs);
            sizeZ = Parser.ReadUInt16(fs);
            unknown1 = Parser.ReadInt16(fs);
            function = (Function)Parser.ReadInt8(fs);
            trigger = (TriggerType)Parser.ReadInt8(fs);
            unknown2 = Parser.ReadInt8(fs);
            unknown3 = Parser.ReadInt8(fs);
            unknown4 = Parser.ReadInt32(fs);
            funcId.ReadStruct(fs);

            int code_Length = Parser.ReadInt32(fs);
            code = Parser.ReadString(fs, code_Length);
        }

        public override void WriteStruct(Stream fs)
        {
            Parser.WriteUInt16(fs, sizeX);
            Parser.WriteUInt16(fs, sizeY);
            Parser.WriteUInt16(fs, sizeZ);
            Parser.WriteInt16(fs, unknown1);
            Parser.WriteInt8(fs, (byte)function);
            Parser.WriteInt8(fs, (byte)trigger);
            Parser.WriteInt8(fs, unknown2);
            Parser.WriteInt8(fs, unknown3);
            Parser.WriteInt32(fs, unknown4);
            funcId.WriteStruct(fs);
            
            Parser.WriteInt32(fs, code.Length);
            Parser.WriteString(fs, code);
        }
    }
}
