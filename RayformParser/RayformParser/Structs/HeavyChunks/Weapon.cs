﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Rayform
{
	// 00 40 00 00, used for procedural weapons
	public class Weapon : HeavyChunk
	{   
		public GripType[] weaponGrips = new GripType[2];
        public GripFlags gripFlags;
		public byte unknown0;
        public Flags flags;
        public int factorySeed;
        public int materialSeed;
		public byte quality;
		public byte wear;
		public byte dirtSmoothing;

		public byte comp_1_Unk1;
		public byte comp_1_Material;
		public byte comp_1_Unk2;
		public byte comp_1_Unk3;
		public byte comp_1_Luster;
		public byte comp_1_Color;
		public byte comp_1_PartUnk4;
		public byte comp_1_PartUnk5;

        public byte comp_2_Unk1;
        public byte comp_2_Material;
        public byte comp_2_Unk2;
        public byte comp_2_Unk3;
        public byte comp_2_Luster;
        public byte comp_2_Color;
        public byte comp_2_PartUnk4;
        public byte comp_2_PartUnk5;

        public byte comp_3_Unk1;
        public byte comp_3_Material;
        public byte comp_3_Unk2;
        public byte comp_3_Unk3;
        public byte comp_3_Luster;
        public byte comp_3_Color;
        public byte comp_3_PartUnk4;
        public byte comp_3_PartUnk5;

        public byte comp_4_Unk1;
        public byte comp_4_Material;
        public byte comp_4_Unk2;
        public byte comp_4_Unk3;
        public byte comp_4_Luster;
        public byte comp_4_Color;
        public byte comp_4_PartUnk4;
        public byte comp_4_PartUnk5;

        public byte unknown25;
        public byte weight;
		public byte balance;
		public byte impactDamage;
		public byte thrustDamage;
        public byte unknown13;
        public byte unknown14;
        public byte unknown15;
        public byte slashDamage;
		public byte crushDamage;
		public byte pierceDamage;
		public bool flip;
		public byte flipImpactDamage;
		public byte flipThrustDamage;
		public byte flipSlashDamage;
		public byte flipCrushDamage;
		public byte flipPierceDamage;
		public byte unknown16;
        public Sound sound;
        public byte rank;
        public byte unknown17;
        public Ability ability; //offset 148
        public byte unknown18; //149
        public byte unknown19; //150
        public byte unknown20; //151
        public Socket[] sockets = new Socket[4]; //152 - 159

        public int unknown21; //component selection start 160

        public byte component1;
        public byte component2;
        public byte component3;
        public byte component4; //163

        public int unknown22; //starts at 164

        public byte compVariant;
        public byte compExtra1; //165
        public byte compExtra2; //166
        public byte compUnknown1; //167

        public int unknown23;
        public int unknown24;

        public enum Flags //tolazyforbitmask
        {
            None = 0,
            HideStats = 1,
            Giant = 2,
            Unknown1 = 3,
            Unknown2 = 48
        }

        public Weapon()
		{
            for (int i = 0; i < 4; i++)
            {
                sockets[i] = new Socket();
            }
        }

		public enum GripType : byte
		{	
			Fists = 0,
			OneHanded = 1,
			TwoHanded = 2,
			Polearm = 3,

		}
        public enum GripFlags : byte
        {
            None = 0,
            Unknown = 1,
            Unknown2 = 2,
            Unknown3 = 3,
			Greatsword = 4
        }
                    public enum Material : byte
            {
				Random = 0,
				ClothRough = 1,
				ClothFine = 2,
				Silk = 3,
                Velvet = 4,
				Padded1 = 5,
				Padded2 = 6,
				Leather1 = 7,
				Leather2 = 8,
				Suede = 9,
                Rawhide = 10,
                ChainMail = 11,
                Steel = 12,
                SteelPlate = 13,
                Painted = 14,
                Bronze = 15,
                Blade = 16,
				WoodFine = 17,
                WoodRough = 18,
                Iron = 19,
				Brass = 20,
				Copper = 21,
				ClothGrip = 22,
				Gemstone = 23,
				Laquer = 24
            }
        public enum Sound
        {
            Sharp1,
            Heavy1,
            Heavy2,
            Sharp2,
            Heavy3,
            OgreMace,
            Wood,
            Clutter,
            Clutter2
        }

        public enum Ability
        {
            None,
            Shock,
            Sharp,
            Fire,
            Sceptre
        }

        public override void ReadStruct(Stream fs)
		{
            Int4[] grips = Parser.ReadInt4(fs, 1);
            weaponGrips[0] = (GripType)(byte)grips[0];
            weaponGrips[1] = (GripType)(byte)grips[1];
			gripFlags = (GripFlags)Parser.ReadInt8(fs);
			unknown0 = Parser.ReadInt8(fs);
			flags = (Flags)Parser.ReadInt8(fs);
            factorySeed = Parser.ReadInt32(fs);
			materialSeed = Parser.ReadInt32(fs);

            quality = Parser.ReadInt8(fs);
            wear = Parser.ReadInt8(fs);
            dirtSmoothing = Parser.ReadInt8(fs);

            comp_1_Unk1 = Parser.ReadInt8(fs);
            comp_1_Material = Parser.ReadInt8(fs);
            comp_1_Unk2 = Parser.ReadInt8(fs);
            comp_1_Unk3 = Parser.ReadInt8(fs);
            comp_1_Luster = Parser.ReadInt8(fs);
            comp_1_Color = Parser.ReadInt8(fs);
            comp_1_PartUnk4 = Parser.ReadInt8(fs);
            comp_1_PartUnk5 = Parser.ReadInt8(fs);

            comp_2_Unk1 = Parser.ReadInt8(fs);
            comp_2_Material = Parser.ReadInt8(fs);
            comp_2_Unk2 = Parser.ReadInt8(fs);
            comp_2_Unk3 = Parser.ReadInt8(fs);
            comp_2_Luster = Parser.ReadInt8(fs);
            comp_2_Color = Parser.ReadInt8(fs);
            comp_2_PartUnk4 = Parser.ReadInt8(fs);
            comp_1_PartUnk5 = Parser.ReadInt8(fs);

            comp_3_Unk1 = Parser.ReadInt8(fs);
            comp_3_Material = Parser.ReadInt8(fs);
            comp_3_Unk2 = Parser.ReadInt8(fs);
            comp_3_Unk3 = Parser.ReadInt8(fs);
            comp_3_Luster = Parser.ReadInt8(fs);
            comp_3_Color = Parser.ReadInt8(fs);
            comp_3_PartUnk4 = Parser.ReadInt8(fs);
            comp_3_PartUnk5 = Parser.ReadInt8(fs);

            comp_4_Unk1 = Parser.ReadInt8(fs);
            comp_4_Material = Parser.ReadInt8(fs);
            comp_4_Unk2 = Parser.ReadInt8(fs);
            comp_4_Unk3 = Parser.ReadInt8(fs);
            comp_4_Luster = Parser.ReadInt8(fs);
            comp_4_Color = Parser.ReadInt8(fs);
            comp_4_PartUnk4 = Parser.ReadInt8(fs);
            comp_4_PartUnk5 = Parser.ReadInt8(fs);

            unknown25 = Parser.ReadInt8(fs);
            weight = Parser.ReadInt8(fs);
            balance = Parser.ReadInt8(fs);
            impactDamage = Parser.ReadInt8(fs);
            thrustDamage = Parser.ReadInt8(fs);
            unknown13 = Parser.ReadInt8(fs);
            unknown14 = Parser.ReadInt8(fs);
            unknown15 = Parser.ReadInt8(fs);
            slashDamage = Parser.ReadInt8(fs);
            crushDamage = Parser.ReadInt8(fs);
            pierceDamage = Parser.ReadInt8(fs);
            flip = Parser.ReadBoolean(fs);
            flipImpactDamage = Parser.ReadInt8(fs);
            flipThrustDamage = Parser.ReadInt8(fs);
            flipSlashDamage = Parser.ReadInt8(fs);
            flipCrushDamage = Parser.ReadInt8(fs);
            flipPierceDamage = Parser.ReadInt8(fs);
            unknown16 = Parser.ReadInt8(fs);
            sound = (Sound)Parser.ReadInt8(fs);
            rank = Parser.ReadInt8(fs);
            unknown17 = Parser.ReadInt8(fs);
            ability = (Ability)Parser.ReadInt8(fs);
            unknown18 = Parser.ReadInt8(fs);
            unknown19 = Parser.ReadInt8(fs);
			unknown20 = Parser.ReadInt8(fs);
			for (int i = 0; i < 4; i++)
			{
				//sockets[i] = new Socket();
                sockets[i].ReadStruct(fs);
			}
            component1 = Parser.ReadInt8(fs);
            component2 = Parser.ReadInt8(fs);
            component3 = Parser.ReadInt8(fs);
            component4 = Parser.ReadInt8(fs);

            compVariant = Parser.ReadInt8(fs);
            compExtra1 = Parser.ReadInt8(fs);
            compExtra2 = Parser.ReadInt8(fs);
            compUnknown1 = Parser.ReadInt8(fs);

            unknown23 = Parser.ReadInt32(fs);
            unknown24 = Parser.ReadInt32(fs);
        }

        public override void WriteStruct(Stream fs)
        {	
            Parser.WriteInt4(fs, new Int4[] { (Int4)(byte)weaponGrips[0], (Int4)(byte)weaponGrips[1] });
			Parser.WriteInt8(fs, (byte)gripFlags);
			Parser.WriteInt8(fs, unknown0);
			Parser.WriteInt8(fs, (byte)flags);
            Parser.WriteInt32(fs, factorySeed);

			Parser.WriteInt32(fs, materialSeed);
            Parser.WriteInt8(fs, quality);
			Parser.WriteInt8(fs, wear);
			Parser.WriteInt8(fs, dirtSmoothing);

			Parser.WriteInt8(fs, comp_1_Unk1);
			Parser.WriteInt8(fs, comp_1_Material);
			Parser.WriteInt8(fs, comp_1_Unk2);
			Parser.WriteInt8(fs, comp_1_Unk3);
			Parser.WriteInt8(fs, comp_1_Luster);
			Parser.WriteInt8(fs, comp_1_Color);
			Parser.WriteInt8(fs, comp_1_PartUnk4);
			Parser.WriteInt8(fs, comp_1_PartUnk5);


            Parser.WriteInt8(fs, comp_2_Unk1);
            Parser.WriteInt8(fs, comp_2_Material);
            Parser.WriteInt8(fs, comp_2_Unk2);
            Parser.WriteInt8(fs, comp_2_Unk3);
            Parser.WriteInt8(fs, comp_2_Luster);
            Parser.WriteInt8(fs, comp_2_Color);
            Parser.WriteInt8(fs, comp_2_PartUnk4);
            Parser.WriteInt8(fs, comp_2_PartUnk5);

            Parser.WriteInt8(fs, comp_3_Unk1);
            Parser.WriteInt8(fs, comp_3_Material);
            Parser.WriteInt8(fs, comp_3_Unk2);
            Parser.WriteInt8(fs, comp_3_Unk3);
            Parser.WriteInt8(fs, comp_3_Luster);
            Parser.WriteInt8(fs, comp_3_Color);
            Parser.WriteInt8(fs, comp_3_PartUnk4);
            Parser.WriteInt8(fs, comp_3_PartUnk5);


            Parser.WriteInt8(fs, comp_4_Unk1);
            Parser.WriteInt8(fs, comp_4_Material);
            Parser.WriteInt8(fs, comp_4_Unk2);
            Parser.WriteInt8(fs, comp_4_Unk3);
            Parser.WriteInt8(fs, comp_4_Luster);
            Parser.WriteInt8(fs, comp_4_Color);
            Parser.WriteInt8(fs, comp_4_PartUnk4);
            Parser.WriteInt8(fs, comp_4_PartUnk5);

            Parser.WriteInt8(fs, unknown25);
            Parser.WriteInt8(fs, weight);
			Parser.WriteInt8(fs, balance);
			Parser.WriteInt8(fs, impactDamage);
			Parser.WriteInt8(fs, thrustDamage);
            Parser.WriteInt8(fs, unknown13);
            Parser.WriteInt8(fs, unknown14);
            Parser.WriteInt8(fs, unknown15);
            Parser.WriteInt8(fs, slashDamage);
			Parser.WriteInt8(fs, crushDamage);
			Parser.WriteInt8(fs, pierceDamage);
			Parser.WriteBoolean(fs, flip);
			Parser.WriteInt8(fs, flipImpactDamage);
			Parser.WriteInt8(fs, flipThrustDamage);
			Parser.WriteInt8(fs, flipSlashDamage);
			Parser.WriteInt8(fs, flipCrushDamage);
			Parser.WriteInt8(fs, flipPierceDamage);
            Parser.WriteInt8(fs, unknown16);
            Parser.WriteInt8(fs, (byte)sound);
            Parser.WriteInt8(fs, rank);
            Parser.WriteInt8(fs, unknown17);
            Parser.WriteInt8(fs, (byte)ability);
            Parser.WriteInt8(fs, unknown18);
            Parser.WriteInt8(fs, unknown19);
            Parser.WriteInt8(fs, unknown20);
            foreach (Socket socket in sockets)
            {
                socket.WriteStruct(fs);
            }
            //Parser.WriteInt32(fs, unknown21);
            Parser.WriteInt8(fs, component1);
            Parser.WriteInt8(fs, component2);
            Parser.WriteInt8(fs, component3);
            Parser.WriteInt8(fs, component4);
            Parser.WriteInt8(fs, compVariant);
            Parser.WriteInt8(fs, compExtra1);
            Parser.WriteInt8(fs, compExtra2);
            Parser.WriteInt8(fs, compUnknown1);
            Parser.WriteInt32(fs, unknown23);
            Parser.WriteInt32(fs, unknown24);
        }
    }
}