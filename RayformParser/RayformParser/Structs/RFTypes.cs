﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using Rayform;
using System;

namespace Rayform
{
    //An array of 16 bytes, used throughout Exanima structures, such as for character names.
    //This facilitates fast string comparisons where all 16 bytes are compared,
    //so these fields (A) are not null-terminated and (B) reserve significance for all bytes,
    //including those after null padding bytes.

    [JsonConverter(typeof(Str128Serializer))]
    public struct Str128
    {
        private string value;

        public Str128(string input)
        {
            if (input != null && input.Length > 16)
            {
                value = input.Substring(0, 16);
                
            }
            else
            {
                value = input;
            }
        }

        public static implicit operator Str128(string input)
        {
            return new Str128(input);
        }

        public override string ToString()
        {
            return value;
        }

        public int Length
        {
            get { return value.Length; }
        }

        public static implicit operator string(Str128 str) => str.value;
    }

    public class Str128Serializer : JsonConverter<Str128>
    {
        public override Str128 ReadJson(JsonReader reader, Type objectType, Str128 existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            return reader.Value.ToString();
        }

        public override void WriteJson(JsonWriter writer, Str128 value, JsonSerializer serializer)
        {
            serializer.Serialize(writer, value.ToString());
        }
    }

    [JsonConverter(typeof(Int4Serializer))]
    public struct Int4
    {
        private byte value;

        public Int4(byte input)
        {
            value = Math.Clamp(input, (byte)0 , (byte)15);
        }

        public static implicit operator Int4(byte input)
        {
            return new Int4(input);
        }

        public static implicit operator byte(Int4 int4) => int4.value;

        public static implicit operator Int4(int input)
        {
            return new Int4((byte)input);
        }

        public override string ToString()
        {
            return Convert.ToString(value);
        }
    }
    
    public class Int4Serializer : JsonConverter<Int4>
    {
        public override Int4 ReadJson(JsonReader reader, Type objectType, Int4 existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            return (Int4)Convert.ToByte(reader.Value);
        }

        public override void WriteJson(JsonWriter writer, Int4 value, JsonSerializer serializer)
        {
            serializer.Serialize(writer, (byte)value);
        }
    }

    //[AttributeUsage(AttributeTargets.Property)]
    //public class FieldSizeAttribute : Attribute
    //{
    //    public Type type;
    //    public string expression;
    //
    //    public FieldSizeAttribute(Type type)
    //    {
    //        this.type = type;
    //    }
    //    public FieldSizeAttribute(string expression)
    //    {
    //        this.expression = expression;
    //    }
    //}
    //
    ////[AttributeUsage(AttributeTargets.Property)]
    //public class HeavyChunkAttribute : Attribute
    //{
    //
    //}

    public struct Color
    {
        //[JsonProperty("Color")]
        public float r;
        public float g;
        public float b;
        public float a;

        public Color(float r, float g, float b, float a)
        {   
            this.r = r;
            this.g = g;
            this.b = b;
            this.a = a;
        }

        //public static implicit operator Color(Vector4 input)
        //{
        //    return new Color(input.W, input.X, input.Y, input.Z);
        //}
        public override string ToString()
        {
            return '(' + r.ToString() + ", " + g.ToString() + ", " + b.ToString() + ", " + a.ToString() + ")";
        }

        //public static implicit operator Vector4(Color color) => color.value;
    }

    public struct Matrix3x4
    {
        public float m00;
        public float m01;
        public float m02;
        public float m10;
        public float m11;
        public float m12;
        public float m20;
        public float m21;
        public float m22;
        public float m30;
        public float m31;
        public float m32;

        public override string ToString()
        {
            return m00.ToString() + ' ' + m01.ToString() + ' ' + m02.ToString() + '\n' + m10.ToString() + ' ' + m11.ToString() + ' ' + m12.ToString() + '\n' + m20.ToString() + +' ' + m21.ToString() + ' ' + m22.ToString() + '\n' + m30.ToString() + ' ' + m31.ToString() + ' ' + m32.ToString();
        }
    }
    public struct Vector2
    {
        public float x;
        public float y;

        public Vector2(float x, float y)
        {
            this.x = x;
            this.y = y;
        }

        public override string ToString()
        {
            return '(' + x + ", " + y + ')';
        }
    }

    public struct Vector3
    {
        public float x;
        public float y;
        public float z;

        public Vector3(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public override string ToString()
        {
            return '(' + x.ToString() + ", " + y.ToString() + ", " + z.ToString() + ')';
        }
    }

    public struct Vector4
    {
        public float x;
        public float y;
        public float z;
        public float w;

        public override string ToString()
        {
            return '(' + x.ToString() + ", " + y.ToString() + ", " + z.ToString() + ", " + w.ToString() + ')';
        }
    }

    //[AttributeUsage(AttributeTargets.Property)]
    //public class SizeFieldTypeAttribute : Attribute
    //{
    //    public Type? type { get; set; }
    //}

    public class RFBinder : ISerializationBinder 
    {
        // On read
        public Type BindToType(string assemblyName, string typeName)
        {
            return Type.GetType(typeName);
        }

        // On write
        public void BindToName(Type serializedType, out string assemblyName, out string typeName)
        {
            assemblyName = null;
            typeName = serializedType.FullName;
        }
    }
}
