﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Rayform
{
    public class Socket : Struct
    {
        public enum Type
        {
            Unsocketed = 0,
            EmptySocket = 128,
            BlueCrystal = 129,
            GreenCrystal = 130,
            Unknown = 131,
            OrangeCrystal = 132,
            RechargeableBlueCrystal = 145
        }

        public Type type;
        public byte crystalUsage;

        public override void ReadStruct(Stream fs)
        {
            type = (Type)Parser.ReadInt8(fs);
            crystalUsage = Parser.ReadInt8(fs);
        }

        public override void WriteStruct(Stream fs)
        {
            Parser.WriteInt8(fs, (byte)type);
            Parser.WriteInt8(fs, crystalUsage);
        }
    }
}