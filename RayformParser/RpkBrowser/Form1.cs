using Rayform;
using System.Diagnostics;
using System.Windows.Forms;

namespace FormTest
{
    public partial class Form1 : Form
    {
        OpenFile openFile;
        Package target;
        public string path;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Icon = Icon.FromHandle(RpkBrowser.Properties.Resources.package.GetHicon());
            //ColumnHeader columnHeader1 = new ColumnHeader();
            //columnHeader1.Text = "Column1";
            //listView1.Columns.AddRange(new ColumnHeader[] { columnHeader1 });
            //file = (Package)Parser.ReadFile("C:\\Program Files (x86)\\Steam\\steamapps\\common\\Exanima\\Resource.rpk");
            ImageList imageList = new ImageList();
            imageList.ImageSize = new Size(24, 24);
            //imageList.TransparentColor = Color.Black;
            imageList.Images.Add("folder", RpkBrowser.Properties.Resources.folder);
            imageList.Images.Add("image", RpkBrowser.Properties.Resources.image);
            imageList.Images.Add("image_folder", RpkBrowser.Properties.Resources.image_folder);
            imageList.Images.Add("object", RpkBrowser.Properties.Resources._object);
            imageList.Images.Add("error", RpkBrowser.Properties.Resources.error);
            imageList.Images.Add("mountain", RpkBrowser.Properties.Resources.mountain);
            imageList.Images.Add("text", RpkBrowser.Properties.Resources.text);
            imageList.Images.Add("media", RpkBrowser.Properties.Resources.media);

            listView1.View = View.Details;
            //listView1.HeaderStyle = ColumnHeaderStyle.None;
            listView1.Columns.Add("Name", 300, HorizontalAlignment.Left);
            listView1.Columns.Add("Size", 300, HorizontalAlignment.Left);
            listView1.Columns.Add("Type", 300, HorizontalAlignment.Left);
            listView1.SmallImageList = imageList;
            listView1.MouseDown += new MouseEventHandler(listView1_MouseDown);
            listView1.MouseDoubleClick += new MouseEventHandler(listView1_MouseDoubleClick);
            Load += new EventHandler(Form1_Load);
            listView1.FullRowSelect = true;
            //listView1.Items.Add(Path.GetFileName(fileName), ImageList1.Images.Count - 1)

            //LoadFile("C:\\Users\\Marcello.Spadaccini\\Desktop\\prj\\files\\Resource.rpk");
            //listView1.Items.Add("...");
            //listView1.Items.Add("actors.rpk", "folder");
            //listView1.Items.Add("ancientbase.rfc", "object");
            //listView1.Items.Add("ancientdifm.rfi", "texture");
            //listView1.Items.Add("ancientmorph.rfc", "object");
        }

        void listView1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ListViewHitTestInfo info = listView1.HitTest(e.X, e.Y);
            ListViewItem item = info.Item;

            if (item != null)
            {
                //MessageBox.Show("The selected Item Name is: " + item.Text);
                Package.Entry file = target.entries.FirstOrDefault(x => x == item.Tag);
                if (file.data != null && file.data.GetType() == typeof(Package))
                {   
                    path = path + '\\' + file.name;
                    textBox1.Text = path;
                    target = (Package)file.data;
                    BuildListView();
                }
            }
            else
            {
                listView1.SelectedItems.Clear();
            }
        }

        void listView1_MouseDown(object sender, MouseEventArgs e)
        {
            ListViewHitTestInfo info = listView1.HitTest(e.X, e.Y);
            ListViewItem item = info.Item;

            //if (item != null)
            //{
            //    //textBox1.Text = item.Text;
            //}
            //else
            //{
            //    listView1.SelectedItems.Clear();
            //    //textBox1.Text = "";
            //}
        }

        public void BuildListView()
        {   
            //Debug.WriteLine("d");
            //FieldInfo[] fields = Utils.GetFields(item);
            listView1.Items.Clear();
            foreach (Package.Entry entry in target.entries)
            {

                ListViewItem item;
                string fileType = "Unknown";



                Debug.WriteLine(path);

                string image = "error";
                if (entry.data != null && entry.data.GetType() == typeof(Package))
                {
                    image = "folder";
                    fileType = "Package";
                }
                else if (entry.extension == ".rfi")
                {
                    image = "image";
                    fileType = "Image";
                }
                else if (entry.extension == ".rfc")
                {
                    image = "object";
                    fileType = "Content";
                }
                else if (entry.extension == ".mat")
                {
                    image = "object";
                    fileType = "Material";
                }
                else if (entry.extension == ".rft")
                {
                    image = "mountain";
                    fileType = "Terrain";
                }
                else if (entry.extension == ".wav")
                {
                    image = "media";
                    fileType = "WAV Audio File";
                }
                else if (entry.extension == ".txt")
                {
                    image = "text";
                    fileType = "Text";
                }
                //else if (fileType == typeof(ImageLibrary))
                //{
                //    item = listView1.Items.Add(entry.name, "image_folder");
                //}
                //else if (fileType == typeof(UnknownChunk))
                //{
                //    item = listView1.Items.Add(entry.name, "error");
                //}
                else
                {
                    image = "text";
                }

                string fileName = entry.name;

                item = listView1.Items.Add(fileName, image);
                item.Tag = entry;
                string sizeString = "";
                if (entry.data == null || entry.data.GetType() != typeof(Package))
                {
                    sizeString = entry.size.ToString("###,###");
                }

                item.SubItems.AddRange(new string[] { sizeString, fileType });
            }
        }

        private void ExtractButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            DialogResult dr = dialog.ShowDialog();

            if (dr == DialogResult.OK)
            {
                //byte[] bytes = File.ReadAllBytes("C:\\Program Files (x86)\\Steam\\steamapps\\common\\Exanima\\Resource.rpk");
                //List<Chunk> chunks = new List<Chunk>();
                for (int i = 0; i < listView1.SelectedItems.Count; i++)
                {
                    //chunks.Add((Chunk)listView1.SelectedItems[i].Tag);
                    Package.Entry entry = (Package.Entry)listView1.SelectedItems[i].Tag;
                    //chunk.ReadStruct2(bytes);
                    ExtractFile(entry, dialog.SelectedPath, path);

                }
                MessageBox.Show("Files successfully extracted");
            }
        }

        private void UpButton_Click(object sender, EventArgs e)
        {
            if (path.Split('\\').Length > 1)
            {
                path = Path.GetDirectoryName(path);
                textBox1.Text = path;

                target = (Package)openFile.data;
                foreach (string entryName in path.Split('\\').Skip(1))
                {
                    target = (Package)target.entries.Where(x => string.Equals(Path.GetFileNameWithoutExtension(x.name), Path.GetFileNameWithoutExtension(entryName), StringComparison.OrdinalIgnoreCase)).FirstOrDefault().data;
                }
                BuildListView();
            }
        }

        private void ExtractAllButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            DialogResult dr = dialog.ShowDialog();

            if (dr == DialogResult.OK)
            {
                foreach (Package.Entry entry in ((Package)openFile.data).entries)
                {
                    ExtractFile(entry, dialog.SelectedPath, "");
                }
                MessageBox.Show("Files successfully extracted");
            }
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            List<Package.Entry> toDelete = listView1.SelectedItems.Cast<ListViewItem>().Select(x => (Package.Entry)x.Tag).ToList();
            DeleteFiles(toDelete);
        }

        private void ImportButton_Click(object sender, EventArgs e)
        {
            List<FileStream> files = new List<FileStream>();
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Multiselect = true;
            DialogResult dr = dialog.ShowDialog();
            
            if (dr == DialogResult.OK)
            {
                foreach(string fileName in dialog.FileNames)
                {
                    files.Add(new FileStream(fileName, FileMode.Open));
                }
                ImportFiles(files);

                MessageBox.Show("Files successfully imported");
            }
        }

        private void LoadButton_Click(object sender, EventArgs e)
        {
            List<FileStream> files = new List<FileStream>();
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Package (*.rpk)|*.rpk";
            DialogResult dr = dialog.ShowDialog();

            if (dr == DialogResult.OK)
            {
                LoadFile(dialog.FileName);
            }
        }

        public void ExtractFile(Package.Entry entry, string path, string innerPath)
        {
            if (entry.data != null && entry.data.GetType() == typeof(Package))
            {
                path += '\\' + entry.name;
                Directory.CreateDirectory(path + innerPath.Replace(openFile.fileName, ""));
                foreach (Package.Entry subEntry in ((Package)entry.data).entries)
                {
                    ExtractFile(subEntry, path, innerPath);
                }
            }
            else
            {

                //string entryName = Path.ChangeExtension(entry.name, entry.extension);

                openFile.fs.Position = entry.position;
                File.WriteAllBytes(path + '\\' + entry.name, Parser.ReadBytes(openFile.fs, (int)entry.size));
            }
        }

        public void DeleteFiles(List<Package.Entry> entries)
        {
            foreach (Package.Entry entry in entries)
            {
                target.entries.Remove(entry);
            }
            ((Package)openFile.data).WriteStruct(openFile.fs);
            BuildListView();
        }

        public void ImportFiles(List<FileStream> files)
        {
            foreach(FileStream file in files)
            {   
                string entryName = Path.GetFileName(file.Name);

                Package.Entry overWrite = target.entries.Where(x => x.name == entryName).FirstOrDefault();
                if (overWrite == null)
                {
                    Package.Entry entry = new Package.Entry();
                    entry.data = file;
                    entry.name = entryName;
                    target.entries.Add(entry);
                }
                else
                {
                    overWrite.data = file;
                }

            }
            target.entries = target.entries.OrderBy(x => x.name.ToString()).ToList();
            ((Package)openFile.data).WriteStruct(openFile.fs);
            BuildListView();
        }

        public void LoadFile(string filePath)
        {
            if(openFile != null)
            {
                openFile.fs.Close();
            }

            openFile = OpenFile.Load(filePath);
            openFile.data = new Package();
            openFile.data.ReadStruct(openFile.fs);
            path = openFile.fileName;
            target = (Package)openFile.data;
            textBox1.Text = path;
            //textBox1.Select(0, 0);
            BuildListView();
        }
    }
}